$(document).ready(function(){
  // OPEN MODAL IMG - TEMPORAL SOLUTION
  $("input.meeta-input.open-modal-img").click(function() {
    $('.meeta-modal-bg').fadeIn("slow");
    $('.meeta-modal-container.img').removeClass("meeta-display-none");
    $('.meeta-modal-container.img').addClass("meeta-display-flex");
  });

  // OPEN MODAL INPUT TEXT - TEMPORAL SOLUTION 

  $("input.meeta-input.open-modal-input-text").click(function() {
    $('.meeta-modal-bg').fadeIn("slow");
    $('.meeta-modal-container.input-text').removeClass("meeta-display-none");
    $('.meeta-modal-container.input-text').addClass("meeta-display-flex");
  });

  // OPEN MODAL TEXTAREA - TEMPORAL SOLUTION

  $("input.meeta-input.open-modal-textarea").click(function() {
    $('.meeta-modal-bg').fadeIn("slow");
    $('.meeta-modal-container.textarea').removeClass("meeta-display-none");
    $('.meeta-modal-container.textarea').addClass("meeta-display-flex");
  });

  // OPEN MODAL PDF - TEMPORAL SOLUTION

  $("input.meeta-input.open-modal-pdf").click(function() {
    $('.meeta-modal-bg').fadeIn("slow");
    $('.meeta-modal-container.pdf').removeClass("meeta-display-none");
    $('.meeta-modal-container.pdf').addClass("meeta-display-flex");
  });
  

  // CLOSE ANY MODAL - TEMPORAL SOLUTION

  $("button.meeta-button.close-modal").click(function() {
    $('.meeta-modal-bg').fadeOut("slow");
    $('.meeta-modal-container').removeClass("meeta-display-flex");
    $('.meeta-modal-container').addClass("meeta-display-none");
  });

});