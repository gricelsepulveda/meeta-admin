const dateFormat = require('dateformat')
const fs = require('fs')
const path = require('path').join(__dirname, 'service.log')
const log_file = fs.createWriteStream(path, { flags : 'w' })

/**
 * @param {string} message 
 * @param {any} arg
 */
log = (message, ...args) => {
  const now = new Date(),
    dateAndMessage = `[${dateFormat(now, 'mmmm dS, yyyy, dddd hh:MM:ss.l')}] ${message}${args.length > 1 ? ': ' + args.splice(0,1).join('\n') : ''}\n`

  log_file.write(dateAndMessage)
  console.log(dateAndMessage)
}

/**
 * @param {string} message 
 * @param {any} args 
 */
exports.log = (message, ...args) => {
  log(message,args)
}

/**
 * @param {string} message 
 * @param {any} args 
 */
exports.error = (message, ...args) => {
  log(`: [!ERR]: ${message}`, args)
}