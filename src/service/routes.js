const path = require('path')

/**
 * Rutas
 * @param {Express} app 
 */
module.exports = function(app, express) {
  const config = require('./controller')

  app.route('/api')
    .get(config.get_config)
    .post(config.write_config)

  app.get('/meeta', (req, res) => {
    const configData = config.get_config_json()
    res.render(path.join(__dirname, '../pages/meeta'), {
      url: configData.url + '/api'
    })
  })

  app.use('/meeta/assets', express.static(path.resolve(__dirname, '../pages', 'assets')))

  app.route('/api/auth')
    .post(config.auth)
}