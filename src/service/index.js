var express = require('express'),
app = express(),
port = process.env.PORT || 8080,
bodyParser = require('body-parser'),
path = require('path'),
routes = require('./routes')
ridentPath = path.join(__dirname, '../pages/rident-landing'),
ridentController = require('./controller')

// PUG viewEngine
app.set('view engine', 'pug')

// CORS - Middleware
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next()
})

// BodyParser
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ limit: '50mb' })) // Lìmite a 50 mb

// Rutas de API
routes(app, express)

//#region Rutas de Rident
app.get('/', (req, res) => {
  /**
   * ! Este valor está en duro, en algún momento será dinámico
   */
  const config = ridentController.config_to_view()
  const configData = ridentController.get_config_json()
  console.log('imagePath', configData.url + 'images/')
  res.render(path.join(ridentPath, 'src/views/index'), {
    config: config,
    imagePath: configData.url+ '/images/',
  })
})

app.get('/especialidades/:titulo', (req, res) => {
  const config = ridentController.config_to_view()
  const configData = ridentController.get_config_json()
  const esp = config['Especialidades'].find(esp => esp.titulo == req.params.titulo)
  if (esp) {
    const indexOfEsp = config['Especialidades'].indexOf(esp)
    res.render(path.join(ridentPath, 'src/views/specialties'), {
      esp,
      config,
      indexOfEsp,
      imagePath: configData.url + '/images/'
    })
  } else {
    return404(req, res)
  }
})

app.get('/sedes/:nombre', (req, res) => {
  const param = (req.params.nombre).trim()
  const config = ridentController.config_to_view()
  const configData = ridentController.get_config_json()
  const sede = config['Sedes'].find(sede => sede.nombre.trim() == param)
  if (sede) {
    const indexOfEsp = config['Sedes'].indexOf(sede)
    res.render(path.join(ridentPath, 'src/views/venues'), {
      sede,
      config,
      indexOfEsp,
      imagePath: configData.url + '/images/'
    })
  } else {
    return404(req, res)
  }
})

//#endregion

app.use('/common', express.static(path.join(ridentPath, '/common')))
app.use('/styles', express.static(path.join(ridentPath, '/dist/styles')))

// Imàgenes de meeta
app.use('/images', express.static(path.join(__dirname, '/images')))
app.use('/docs', express.static(path.join(__dirname, '/docs')))


function return404(req,res) {
  res.render(path.join(__dirname, '../pages/404'))
}

app.get('*', return404);

app.listen(port)
console.log(`Magic happening on ${port}`)
