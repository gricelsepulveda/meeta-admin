import * as React from 'react'
import '@styles/meeta-admin-styles.scss'

export default class Login extends React.Component<any, any> {
  constructor(props:any) {
    super(props)

    this.authenticate = this.authenticate.bind(this)
  }

  user: HTMLInputElement
  pass: HTMLInputElement

  authenticate() {
    this.props.authenticate(this.user.value, this.pass.value)
  }

  render() {
    return(
      <div className="meeta-login meeta-col">
        <div className="meeta-col">
          <span className="meeta-logo">me<span className="highlight">e</span>ta</span><span className="meeta-logo-after">Administrador de contenidos</span>
        </div>
        <div className="meeta-wrapper dark width-260px title lg login">
          <h1 className="meeta-h1 width-260px lg">
            Bienvenido
            </h1>
          <form className="meeta-form">
            <div className="meeta-col start">
              <div className="meeta-element-container width-100">
                <label className="meeta-label dark">Usuario:</label><input autoFocus className="meeta-input dark" type="text" ref={ref => this.user = ref}/>
              </div>
                <div className="meeta-element-container width-100">
                  <label className="meeta-label dark">Contraseña:</label><input className="meeta-input dark" type="password" ref={ref => this.pass = ref}/>
                </div>
                  <div className="meeta-element-container width-100">
                    <button className="meeta-button dark sm" type="button" onClick={this.authenticate}>Ingresar</button>
                  </div>
                </div>
            </form>
              <span className="meeta-note margin-footer">
                <svg aria-hidden="true" className="svg-inline--fa fa-exclamation-circle fa-w-16" data-fa-processed="" data-prefix="fas" data-icon="exclamation-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                  <path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path>
                </svg>
                ¿Problemas con tu contraseña?<a href="mailto:soporte@meeta.cl">Escribenos</a>
                </span>
            </div>
        </div>
    )
  }
}