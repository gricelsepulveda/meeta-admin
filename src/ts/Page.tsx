import * as React from 'react'
import * as Layout from '@ts/components/layout'
import { MenuEntry } from '@ts/components/layout'
import Form, { IFormProps, FormEntry } from '@ts/components/form'
import { IPageContainerState } from '@ts/App';

export interface IPageStructure {
  identifier: string
  formProps: IFormProps[]
  maxEntryAmount?: number
  minEntryAmount?: number
}

export interface IPageState {
  pages: IPageStructure[]
}

export class Page extends React.Component<IPageContainerState & { onSubmit: (inputs: FormEntry[], propIndex: number, ) => void }, IPageState> {
  constructor(props) {
    super(props)
    this.state = {
      pages: [...this.props.structure.pages]
    }
    this.destroyFormProp = this.destroyFormProp.bind(this)
    this.getSelectedpage = this.getSelectedpage.bind(this)
    this.addFormProp = this.addFormProp.bind(this)
    this.getIndexOfSelectedPage = this.getIndexOfSelectedPage.bind(this)
  }

  componentWillReceiveProps(nextProps: IPageContainerState) {
    this.setState({
      pages: [...nextProps.structure.pages]
    })
  }

  getSelectedpage() {
    return {...this.state.pages.slice().find(page => page.identifier == this.props.selectedPageIdentifier)}
  }

  getIndexOfSelectedPage() {
    return this.state.pages.slice().findIndex(page => page.identifier == this.props.selectedPageIdentifier)
  }

  /**
   * @param index Eliminado
   */
  destroyFormProp(index: number) {
    let newFormProps = Object.assign([], this.getSelectedpage().formProps)
    newFormProps = newFormProps.map((formProp: IFormProps, formPropIndex) => {
      if (formPropIndex >= index) {
        formProp.sub = formProp.sub.replace(String(formPropIndex + 1), String(formPropIndex))
        formProp.rows.map(row => {
          row.cols.forEach(col => {
            col.name = col.name.replace(String(formPropIndex + 1), String(formPropIndex))
          })
        });
      }
      return formProp
    });
    newFormProps.splice(index, 1)
    let newPagesState = this.state.pages
    newPagesState[this.getIndexOfSelectedPage()].formProps = newFormProps
    this.setState({
      pages: newPagesState
    })
  }

  addFormProp() {
    /** Toda esta paja de object assign es por que javascript es una mierda haciendo copias de arrays,
     * tienes que hacerlo a todos los niveles por que si no guarda las referencias y agrega al peo,
     * modificando el objeto de referencia y dejando la cagá
     */
    //#region  Cáncer
    let newIndex = this.getSelectedpage().formProps.length + 1
    let pages: IPageStructure[] = Object.assign([], this.state.pages)
    let selectedpage = Object.assign([], pages[this.getIndexOfSelectedPage()] )
    let formProps = Object.assign([], selectedpage.formProps)                         
    let newFormProp = Object.assign({}, formProps[0])
    
    //#endregion
    newFormProp.sub = newFormProp.sub.replace("1", String(newIndex))

    newFormProp.rows = newFormProp.rows.slice().map(row => {  
      let newRow = Object.assign({}, row)
      newRow.cols = row.cols.slice().map(col => {
        let newCol = Object.assign({}, col)
        newCol.name = newCol.name.replace("1", String(newIndex))
        delete newCol.value
        return newCol
      })
      return newRow
    })
    
    console.log(newFormProp)
    pages[this.getIndexOfSelectedPage()].formProps.push(newFormProp)

    this.setState({
      pages
    })
  }

  render() {
    const selectedPage = this.getSelectedpage(),
      currentEntryAmount = selectedPage.formProps.length,
      canAddNewEntries = currentEntryAmount < selectedPage.maxEntryAmount,
      canDeleteEntries = currentEntryAmount > selectedPage.minEntryAmount

    return (
      <Layout.Main logout={this.props.logout} menuStructure={this.props.structure.menu} clientName={this.props.structure.clientName} selectedIdentifier={this.props.selectedPageIdentifier}>
        {
          /**
           * Botòn de agregación
           */
          (canAddNewEntries) ?
            <button onClick={this.addFormProp} className="meeta-button light control plus"></button>
          :
            null
        }
        {
          /**
           * Por cada formProp crea una entrada de Form
           */
          selectedPage.formProps.map((formProps, index) => {
            return (
              <Form
                {...formProps}
                canDeleteEntries={canDeleteEntries}
                destroyFormProp={() => {
                  if (confirm(`¿Estás seguro que deseas eliminar ${formProps.sub}?`)) {
                    this.destroyFormProp(index)
                  }
                }}
                onSubmit={(inputs: FormEntry[]) => { this.props.onSubmit(inputs, index) }}
              />
            )
          }
          )
        }
      </Layout.Main>
    )
  }
}
