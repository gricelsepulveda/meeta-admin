import * as React from 'react'
import * as ReactDOM from 'react-dom'
import '@styles/normalize.css'
import '@styles/font-awesome.css'
import { IPageStructure, Page } from '@ts/Page'
import { MenuEntry } from '@ts/components/layout'
import { FormEntry, IFormProps } from '@ts/components/form'
import Login from '@ts/Login';
import *  as md5 from 'js-md5'
import { ToastContainer, toast } from 'react-toastify'

/** Clase auxiliar de mapeado de propiedades para crear el menú */
class AppStructure implements IAppConfigStructure {
  clientName: string;
  url: string
  pages: IPageStructure[]
  menu: MenuEntry[]

  constructor(props: IAppConfigStructure, setSelectedIdentifier: Function) {
    this.clientName = props.clientName,
    this.pages = props.pages
    this.url = props.url
    this.menu = props.pages.map(structure => new MenuEntry(structure.identifier, () =>  setSelectedIdentifier(structure.identifier) ))
  }
}

export interface IAppConfigStructure {
  clientName: string
  url: string
  pages: IPageStructure[]
  menu?: MenuEntry[],
}

export interface IPageContainerState {
  selectedPageIdentifier: string
  structure: AppStructure,
  isAuthenticated: boolean
  logout?: Function
}

/** Contenedor de funcionalidad */
class PageContainer extends React.Component<IAppConfigStructure, IPageContainerState> {

  constructor(props: IAppConfigStructure) {
    super(props)
    
    this.onSubmit = this.onSubmit.bind(this)
    this.setSelectedIdentifier = this.setSelectedIdentifier.bind(this)
    this.sendData = this.sendData.bind(this)
    this.authenticate = this.authenticate.bind(this)
    this.logout = this.logout.bind(this)

    let structure = new AppStructure(this.props, this.setSelectedIdentifier )

    this.state = {
      selectedPageIdentifier: structure.pages[0].identifier,
      structure,
      isAuthenticated: false
    }
  }

  onSubmit(inputs: FormEntry[], formPropIndex: number, selectedIdentifier: string = this.state.selectedPageIdentifier): void {
    const selectedPageInstance = this.state.structure.pages.find(page => page.identifier == selectedIdentifier)
    const updatedFormProps = this.getUpdatedFormProp(selectedPageInstance.formProps[formPropIndex], inputs)
    const indexOfSelectedPage = this.state.structure.pages.findIndex(page => page.identifier == selectedIdentifier)

    let structure = this.state.structure
    structure.pages[indexOfSelectedPage] = Object.assign({}, structure.pages[indexOfSelectedPage], selectedPageInstance)
    this.sendData(structure)
  }

  sendData(newStructure: IAppConfigStructure) {
    const apiCopy = Object.assign({}, newStructure, { menu: null})
    fetch(this.props.url + '/api', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(apiCopy),
      method: 'POST',
    }).then(response => {
      // Cambio aceptado
      if (response.ok) {
        let structure = this.state.structure;
        structure.pages = newStructure.pages
        this.setState({ structure: structure })
        toast('Datos actualizados exitosamente', { type: 'success' })
      } else {
        toast('Oops! Ocurrió un error al intentar actualizar los datos', { type: 'error' })
      }
    }).catch(error => console.error(error))
  }

  setSelectedIdentifier(identifier: string) {
    this.setState({ selectedPageIdentifier: identifier })
  }

  authenticate(user:string, pass:string) {
    const payload = {
      user,
      pass: md5.create().update(pass).hex()
    }

    fetch(this.props.url + '/api/auth', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload),
      method: 'POST',
    }).then(response => {
      if (response.ok) {
        this.setState({ isAuthenticated: true })
      } else {
        toast('Credenciales inválidas', { type: 'error'})
      }
    })
  }

  logout() {
    this.setState({ isAuthenticated: false })
  }

  getUpdatedFormProp(formProps: IFormProps, entries: FormEntry[]): IFormProps {
    console.log(formProps, entries)
    let output: IFormProps = Object.assign({}, formProps)
    for (let formEntry of entries) {
      output.rows[formEntry.rowIndex].cols[formEntry.colIndex].value = formEntry.value
    }
    return output
  }

  render() {
    if (!this.state.isAuthenticated) return <div><Login authenticate={this.authenticate}/><ToastContainer /></div>
    return ([
      <Page logout={this.logout} { ...this.state } onSubmit={this.onSubmit} key="appPage"/>,
      <ToastContainer />
    ])
  }
}

export let AppUrl

export function render(config?: IAppConfigStructure) {
  ReactDOM.render(React.createElement(PageContainer, config), document.getElementById('app'), () => AppUrl = config.url)
}
