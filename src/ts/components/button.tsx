import * as React from 'react'
import '@styles/meeta-admin-button'

export interface IButtonProps {
  color: ButtonColors
  size: ButtonSizes
  kind?: ButtonKinds
  active?: boolean
  onClick: any
}

export enum ButtonColors {
  'light','sky'
}

export enum ButtonSizes {
  'sm', 'md', 'lg'
}

export enum ButtonKinds {
  'square'
}

export default class Button extends React.Component<IButtonProps, any> {
  render() {
    return (
      <button
        className={`meeta-button ${ButtonColors[this.props.color]} ${ButtonKinds[this.props.kind] ? ButtonKinds[this.props.kind] : ''} ${ButtonSizes[this.props.size]} ${this.props.active ? 'active' : ''}`}
        type="button"
        onClick={this.props.onClick}
      >
        {this.props.children}
      </button>
    )
  }
}