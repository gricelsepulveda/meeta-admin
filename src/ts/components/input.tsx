import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Modal from '@ts/components/modal'


require('@styles/meeta-admin-input-file')
require('@styles/meeta-admin-input')

interface IInputProps {
  name: string
  placeholder?: string
  value?: any
}

interface IInputState {
  editModalOpen: boolean
  value: string
}

export default class Input extends React.Component<IInputProps, IInputState> {
  input: HTMLElement

  constructor(props: any) {
    super(props)

    this.state = {
      editModalOpen: false,
      value: this.props.value ? this.props.value : '',
    }

    this.setModalOpenState = this.setModalOpenState.bind(this)
    this.setValue = this.setValue.bind(this)
  }

  setModalOpenState(newState: boolean) {
    this.setState({ editModalOpen: newState })
  }

  setValue(newValue: string) {
    this.setState({ value: newValue })
  }

  render() {
    return (
      [
        <input
          className="meeta-input dark open-modal-textarea"
          key={'theReal' + this.props.name}
          name={this.props.name}
          type="text"
          data-type="text"
          ref={ref => this.input = ref}
          placeholder={this.props.placeholder}
          onClick={() => this.setModalOpenState(true)}
          value={this.state.value}
          readOnly
        />,
        this.state.editModalOpen ? // Modal de edición
          <Modal
            key={'theModal' + this.props.name}
            title="Actualizar contenido"
            size="md"
            type="text"
            inputRef={this.input}
            onClose={(value:any) => { this.setModalOpenState(false); value? this.setValue(value) : null }}
          />
          :
          null
      ]
    )
  }
}
