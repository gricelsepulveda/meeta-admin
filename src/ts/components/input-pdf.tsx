import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Modal from '@ts/components/modal';

require('@styles/meeta-admin-input-file')
require('@styles/meeta-admin-input')

interface IInputFileProps {
  name: string
  placeholder?: string
  value?: any
}

interface IInputFileState {
  fileName: string,
  value: any
}

export default class InputFilePDF extends React.Component<IInputFileProps, IInputFileState> {
  input: HTMLElement

  constructor(props: IInputFileProps) {
    super(props)

    this.state = {
      fileName: this.props.value ? this.props.value : '',
      value: this.props.value ? this.props.value : ''
    }

    this.setFile = this.setFile.bind(this)
  }

  setFile(value: HTMLInputElement) {
    const fileName = value.files[0].name
    fileHash(value.files[0]).then(hash => {
      this.setState({
        fileName: fileName,
        value: hash
      })
    })

  }

  render() {
    return (
      [
        <input // Máscara
          key={'theFake' + this.props.name}
          className="meeta-input pdf dark open-modal-img"
          type="text"
          value={this.state.fileName}
          placeholder={this.props.placeholder}
          onClick={() => this.input.click()}
          readOnly
        />,
        <input
          key={'theRealReal' + this.props.name}
          className="meeta-display-none"
          data-type="file"
          name={this.props.name}
          value={this.state.value}
        />
        ,
        <input // Input de recopilación de datos, se usa de referencia para el loader
          key={'theReal' + this.props.name}
          className="meeta-display-none"
          ref={ref => this.input = ref}
          type="file"
          accept=".pdf"
          onChange={(event: any) => this.setFile(event.target)}
        />,
      ]
    )
  }
}


function fileHash(file: File) {
  return new Promise((resolve, reject) => {
    if (!file)
      reject('No file provided')
    try {
      let fr = new FileReader()
      fr.onload = (reader: any) => {
        resolve(reader.target.result)
      }
      fr.readAsDataURL(file)
    } catch (error) {
      console.error('Error when hashing file', error)
    }
  })
}
