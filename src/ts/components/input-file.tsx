import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Modal from '@ts/components/modal'
import { AppUrl } from "@ts/App"

require('@styles/meeta-admin-input-file')
require('@styles/meeta-admin-input')

interface IInputFileProps {
  name: string
  placeholder?: string
  value?: any
}

interface IInputFileState {
  editModalOpen: boolean,
  fileName: string,
  value: any
}

export default class InputFile extends React.Component<IInputFileProps, IInputFileState> {
  input: HTMLElement
  
  constructor(props: IInputFileProps) {
    super(props)

    this.state = {
      editModalOpen: false,
      fileName: this.props.value ? this.props.value : '',
      value: this.props.value ? this.props.value : ''
    }

    this.setModalOpenState = this.setModalOpenState.bind(this)
    this.setFile = this.setFile.bind(this)
  }

  setModalOpenState(newState: boolean = !this.state.editModalOpen) {
    this.setState({ editModalOpen: newState })
  }

  setFile(value: HTMLInputElement) {
    fileHash(value.files[0]).then(hash => {
      this.setState({
        fileName: value.files[0].name,
        value: hash
      })
    })
    
  }

  render () {
    return (
      [
        <input // Máscara
          key={'theFake'+this.props.name}
          className="meeta-input dark open-modal-img"
          type="text"
          value={this.state.fileName}
          placeholder={this.props.placeholder}
          onClick={() => this.setModalOpenState(true)}
          readOnly
        />,
        <input
          key={'theRealReal'+this.props.name}
          className="meeta-display-none"
          data-type="file"
          name={this.props.name}
          value={this.state.value}
        />
        ,
        <input // Input de recopilación de datos, se usa de referencia para el loader
          key={'theReal'+this.props.name}
          className="meeta-display-none"
          ref={ref => this.input = ref}
          type="file"
          accept="image/*"
          onChange={(event: any) => this.setFile(event.target) }
        />,
        this.state.editModalOpen ? // Modal de edición
          <Modal
            key={'theModal'+this.props.name}
            title="Actualizar contenido"
            size="sm"
            value={`${AppUrl}/images/${this.state.fileName}`}
            type="image"
            inputRef={this.input}
            onClose={() => this.setModalOpenState(false) }
          />
          :
          null
      ]
    )
  }
}


function fileHash(file: File) {
  return new Promise((resolve, reject) => {
    if (!file)
      reject('No file provided')
    try {
      let fr = new FileReader()
      fr.onload = (reader: any) => {
        resolve(reader.target.result)
      }
      fr.readAsDataURL(file)
    } catch (error) {
      console.error('Error when hashing file', error)
    }
  })
}
