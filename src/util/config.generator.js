// Aplicacion para crear rápidamente archivos de configuración vírgenes
const path = require('path'),
fs = require('fs'),
yaml = require('js-yaml'),
inputFilePath = path.join(__dirname, 'input.yml'),
outputFilePath = path.join(__dirname, 'output.json')

/**
 * 
 * @param {string} subTitle 
 */
const normalizeSubTitle = (subTitle) => subTitle.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase()

const createFormProps = (amount, page, dataSource) => {
  let pageStructure = {
    identifier: page.identifier,
    formProps: [],
    minEntryAmount: page.minEntryAmount,
    maxEntryAmount: page.maxEntryAmount
  }
  for (let i = 1; i <= amount; i++) {
    let rows = []
    page.formProps.colStructure.forEach(colStructure => {
      let columns = []
      Object.keys(colStructure).forEach(propName => {
        const colDefinition = colStructure[propName]
        const colName = `${normalizeSubTitle(page.formProps.subTitle)}${i}${propName}`
        const col = {
          name: colName,
          label: colDefinition.label,
          type: colDefinition.type,
          placeholder: colDefinition.placeholder,
          width: colDefinition.width
        }
        if (dataSource && dataSource[colName])
          col.value = dataSource[colName]
        columns.push(col)
      })
      rows.push({ "cols": columns })
    })
    pageStructure.formProps.push({
      sub: `${page.formProps.subTitle} #${i}`,
      rows: rows
    })
  }
  return pageStructure
}

try {
  console.log('Leyendo', inputFilePath)
  const doc = yaml.safeLoad(fs.readFileSync(inputFilePath, 'utf8'))
  let output = {
    clientName: doc.clientName,
    pages: doc.pages.map(page => createFormProps(page.entryAmount, page)),
    url: doc.url
  }

  fs.writeFile(outputFilePath, JSON.stringify(output), 'utf8')
} catch (error) {
  console.error(error)
}

module.exports = {
  normalizeSubTitle,
  createFormProps
} 
