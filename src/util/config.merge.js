let args = {}
const fs = require('fs')
const path = require('path')
const basePath = path.resolve(__dirname, '../..')
const yaml = require('js-yaml')
const yamlFile = path.join(__dirname, 'input.yml')

// Mapeo de parámetros
process.argv.slice(2).forEach((value, index, array) => {
  if (index % 2 == 0) { // Busca parámetros pareados
    args[array[index].replace('-', '')] = array[index + 1]
  }
});

/** Plantilla */
const yamlTemplate = yaml.safeLoad(fs.readFileSync(yamlFile, 'utf8'))
/** Archivo con los datos completados */
let source = JSON.parse(fs.readFileSync(path.resolve(basePath, args.s), 'utf8'))
/** Valor de salida */
let output = Object.assign({}, source)

/**
 *  Toma todas las configuraciones de un FormProp y los coloca en un objeto plano
 */
const flattenValues = (formProps) => {
  const output = {}
  let keys = []
  if (!formProps) return output

  formProps.forEach((formProp, index) => {
    formProp.rows.forEach((row) => {
      row.cols.forEach((col) => {
        output[col.name] = col.value
      })
    })
  })
  return output
}

/**
 * 
 * @param {string} identifier 
 */
const findPage = identifier => yamlTemplate.pages.find(page => page.identifier === identifier)

const createFormProps = require('./config.generator').createFormProps

output.pages = source.pages.map((sPage, pageIndex) => {
  const page = findPage(sPage.identifier)
  if (page) {
    const pageFlatValues = flattenValues(sPage.formProps)
    const amount = sPage.formProps.length
    console.log(sPage.identifier, amount)
    sPage = createFormProps(amount, page, pageFlatValues)
  }
  return sPage
})

fs.writeFileSync(__dirname + '/merge.json', JSON.stringify(output))
