const webpack = require('webpack')
const path = require('path')
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssets = require('optimize-css-assets-webpack-plugin')


const config = {
  entry: './src/ts/App.tsx',
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, './src/pages/assets'),
    library: "meeta",
  },

  devtool: "inline-source-map",

  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json", ".scss"],
    alias: {
      "@views": path.resolve(__dirname, 'src', 'views'),
      "@ts": path.resolve(__dirname, 'src', 'ts'),
      "@styles": path.resolve(__dirname, 'src', 'styles')
    }
  },

  module: {
    rules: [
      { // TSX
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader"
      },
      { // SASS
        test: /\.scss$/,
        use: ['css-hot-loader'].concat(ExtractTextWebpackPlugin.extract({ // llamado al plugin
          use: ['css-loader', 'sass-loader', 'postcss-loader'], // utilizar estos loaders
          fallback: 'style-loader', // fallback si no se utliza ninguno
        })),
      },
      { // CSS
        test: /\.css$/,
        use: ExtractTextWebpackPlugin.extract([
          {
            loader: 'css-loader',
          }
        ])
      },
      { // JS
        enforce: "pre",
        test: /\.js$/, loader: "source-map-loader"
      },
      { // IMÁGENES
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: ['file-loader?context=src/assets/images/&name=images/[path][name].[ext]', { 
          loader: 'image-webpack-loader',
          query: {
            mozjpeg: {
              progressive: true,
            },
            gifsicle: {
              interlaced: false,
            },
            optipng: {
              optimizationLevel: 4,
            },
            pngquant: {
              quality: '75-90',
              speed: 3,
            },
          },
        }],
        exclude: /node_modules/,
        include: __dirname,
      },
      { // FONTS
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loaders: ['file-loader?name=fonts/[name].[ext]'],
      },

    ]
  },

  plugins: [
    new ExtractTextWebpackPlugin('styles.css'), // llama al constructor y define el nombre del archivo
  ],

  devServer: {
    contentBase: path.resolve(__dirname, './public'), // directorio o URL para servir
    historyApiFallback: true, // fallback hacia index.html para aplicaciones de una sóla pàgina
    inline: true, // mode en linea (configurar en falso para deshabilitar script en cliente, ej: livereload)
    open: true, // abre por defecto el navegador
    port: 9000,
  },
}

module.exports = config

// Configuraciòn de ambiente de producción
if (process.env.NODE_ENV === 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin(),
    new OptimizeCSSAssets(),
  )
  module.exports.devtool = false
}
